package strings;

import strings.model.Text;
import strings.task.Task;
import strings.utils.ConsoleReader;
import strings.utils.TextParser;
import strings.utils.TextReader;

public class Application {
    ConsoleReader consoleReader = new ConsoleReader();
    TextReader textReader = new TextReader();
    Text text = new Text(new TextParser().findSentencesInText(textReader.readFirstLineFromFile("src/main/resources/file.txt")));
    TaskFabric taskFabric = new TaskFabric(text);
    Menu menu = new Menu();

    public static void main(String[] args) {
        new Application().runApplication();

    }

    public void runApplication() {
        int value;
        do {
            value = consoleReader.readValue(menu.getMenu(), menu.getMenuLength());
            Task task = taskFabric.getTask(value);
            task.executeTask();
        } while (value != 0);

    }
}


