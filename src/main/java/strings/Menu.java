package strings;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import strings.utils.PropertiesHandler;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class Menu {
    private static Logger LOG = LogManager.getLogger(Menu.class);
    private Map<Integer, String> menu = new HashMap<>();

    private void createMenu() {
        Locale locale = new Locale(PropertiesHandler.getLocale());
        ResourceBundle bundle = ResourceBundle.getBundle("MyMenu", locale);
        menu.put(0, bundle.getString("0"));
        menu.put(1, bundle.getString("1"));
        menu.put(2, bundle.getString("2"));
        menu.put(3, bundle.getString("3"));
        menu.put(4, bundle.getString("4"));
        menu.put(5, bundle.getString("5"));
        menu.put(6, bundle.getString("6"));
        menu.put(7, bundle.getString("7"));
        menu.put(8, bundle.getString("8"));
    }

    public String getMenu() {
        createMenu();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Please, choose task\n");
        menu.forEach((key, value) -> stringBuilder.append(key + " - " + value + "\n"));
        return stringBuilder.toString();
    }

    public int getMenuLength() {
        return menu.size();
    }
}
