package strings;

import strings.model.Text;
import strings.task.*;

import java.util.HashMap;
import java.util.Map;

public class TaskFabric {
    private Map<Integer, Task> taskMap = new HashMap<>();

    TaskFabric(Text text) {
        taskMap.put(1, new Task1(text));
        taskMap.put(2, new Task2(text));
        taskMap.put(3, new Task3(text));
        taskMap.put(4, new Task4(text));
        taskMap.put(5, new Task5(text));
        taskMap.put(6, new Task6(text));
        taskMap.put(7, new Task7());
        taskMap.put(8, new Task8());
    }

    Task getTask(int value) {
        return taskMap.get(value);
    }

}
