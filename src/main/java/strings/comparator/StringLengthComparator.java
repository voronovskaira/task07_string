package strings.comparator;

import strings.model.Word;

import java.util.Comparator;

public class StringLengthComparator implements Comparator<Word> {
    @Override
    public int compare(Word o1, Word o2) {
        return o1.getWord().length() - o2.getWord().length();
    }
}
