package strings.comparator;

import strings.model.Sentence;

import java.util.Comparator;

public class WordCountComparator implements Comparator<Sentence> {
    @Override
    public int compare(Sentence o1, Sentence o2) {
        return o1.getWords().size() - o2.getWords().size();
    }
}
