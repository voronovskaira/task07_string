package strings.constant;

public class RegexConstants {
    public static String VOWEL_WORDS = "(^[aieyuo]\\w+)";
    public static String CONSONANT_WORDS = "(^[^aieyuo]\\w+)";
    public static String SENTENCE_REGEX = "\\s+[^.!?]*[.!?]";
    public static String WORD_REGEX = "\\w+";
}
