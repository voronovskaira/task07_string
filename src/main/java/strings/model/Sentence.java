package strings.model;

import java.util.List;

public class Sentence {
    private String sentence;
    private List<Word> words;

    public Sentence(String sentence) {
        this.sentence = sentence;
    }

    public Sentence(String sentence, List<Word> words) {
        this.sentence = sentence;
        this.words = words;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public List<Word> getWords() {
        return words;
    }

    @Override
    public String toString() {
        return sentence;
    }
}
