package strings.model;

import java.util.ArrayList;
import java.util.List;

public class Text {
    List<Sentence> sentences;

    public Text(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    public List<Word> getWords(){
        List<Word> words = new ArrayList<>();
        sentences.forEach(sentence -> words.addAll(sentence.getWords()));
        return words;
    }
}
