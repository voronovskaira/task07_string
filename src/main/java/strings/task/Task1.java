package strings.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import strings.model.Sentence;
import strings.model.Text;
import strings.model.Word;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Task1 implements Task {

    private Text text;
    private static Logger LOG = LogManager.getLogger(Task1.class);

    public Task1(Text text) {
        this.text = text;
    }

    public void executeTask() {
        List<Sentence> sentencesWithDuplication = text.getSentences()
                .stream()
                .filter(this::hasDuplicatedWords).collect(Collectors.toList());
        sentencesWithDuplication.forEach(sentence -> LOG.info("Duplicated sentence: " + sentence));
        LOG.info("in text is " + sentencesWithDuplication.size() + " duplicated sentences");
    }

    private boolean hasDuplicatedWords(Sentence sentence) {
        Set<Word> distinctWords = new HashSet<>(sentence.getWords());
        return sentence.getWords().size() > distinctWords.size();
    }

}
