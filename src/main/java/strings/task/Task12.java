package strings.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import strings.constant.RegexConstants;
import strings.model.Text;
import strings.model.Word;

import java.util.List;
import java.util.stream.Collectors;

public class Task12 {
    private Text text;
    private static Logger LOG = LogManager.getLogger(Task12.class);

    public Task12(Text text) {
        this.text = text;
    }

    public void executeTask(int length) {
        text.getSentences().forEach(sentence -> {
            List<Word> consonantsWords = getConsonantsWords(sentence.getWords(), length);
            consonantsWords.forEach(word ->
                    sentence.setSentence(sentence.getSentence().replace(word.getWord(), "")));
            LOG.info(sentence);
        });
    }

    private List<Word> getConsonantsWords(List<Word> list, int length) {
        return list.stream()
                .filter(word -> word.getWord().toLowerCase().matches(RegexConstants.CONSONANT_WORDS))
                .filter(word -> word.getWord().length() == length)
                .collect(Collectors.toList());
    }
}
