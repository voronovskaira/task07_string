package strings.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import strings.comparator.WordCountComparator;
import strings.model.Text;

public class Task2 implements Task {
    private Text text;
    private static Logger LOG = LogManager.getLogger(Task2.class);

    public Task2(Text text) {
        this.text = text;
    }

    public void executeTask() {
        text.getSentences().stream().sorted(new WordCountComparator()).forEach(x -> LOG.info(x));
    }
}
