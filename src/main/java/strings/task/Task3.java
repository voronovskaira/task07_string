package strings.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import strings.model.Text;
import strings.model.Word;

public class Task3 implements Task {
    private Text text;
    private static Logger LOG = LogManager.getLogger(Task3.class);

    public Task3(Text text) {
        this.text = text;
    }

    public void executeTask() {
        text.getSentences().get(0).getWords().stream().filter(this::containsInOtherSentences).forEach(LOG::info);
    }

    private boolean containsInOtherSentences(Word word) {
        boolean isContains = true;
        for (int i = 1; i < text.getSentences().size(); i++) {
            if (text.getSentences().get(i).getWords().contains(word))
                isContains = false;
        }
        return isContains;
    }
}
