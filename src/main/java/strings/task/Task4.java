package strings.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import strings.model.Sentence;
import strings.model.Text;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Task4 implements Task {
    private Text text;
    private static Logger LOG = LogManager.getLogger(Task4.class);
    private static Scanner scan = new Scanner(System.in);

    public Task4(Text text) {
        this.text = text;
    }

    public void executeTask() {
        LOG.info("Input length");
        int length = scan.nextInt();
        List<Sentence> questionSentences = text.getSentences().stream()
                .filter(sentence -> sentence.getSentence().endsWith("?"))
                .collect(Collectors.toList());
        questionSentences
                .forEach(sentence -> sentence.getWords().stream()
                        .filter(word -> word.getWord().length() == length).forEach(LOG::info));
    }

}
