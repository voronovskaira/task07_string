package strings.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import strings.model.Text;
import strings.model.Word;

import java.util.List;

public class Task5 implements Task {
    private Text text;
    private static Logger LOG = LogManager.getLogger(Task5.class);

    public Task5(Text text) {
        this.text = text;
    }

    public void executeTask() {
        List<Word> words = text.getWords();
        words.sort(Word::compareTo);
        words.forEach(LOG::info);
    }
}
