package strings.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import strings.comparator.StringLengthComparator;
import strings.constant.RegexConstants;
import strings.model.Text;
import strings.model.Word;

import java.util.List;
import java.util.stream.Collectors;

public class Task6 implements Task {
    private Text text;
    private static Logger LOG = LogManager.getLogger(Task6.class);

    public Task6(Text text) {
        this.text = text;
    }

    public void executeTask() {
        text.getSentences().forEach(sentence -> {
            String wordBeginOfVowelLetter = wordBeginOfVowelLetter(sentence.getWords());
            String longestWord = longestWord(sentence.getWords());
            sentence.getSentence().replace(longestWord, wordBeginOfVowelLetter);
            sentence.setSentence(sentence.getSentence().replaceFirst(wordBeginOfVowelLetter, longestWord));
            LOG.info(sentence);
        });
    }

    public String wordBeginOfVowelLetter(List<Word> list) {
        List<Word> wordsOnVowel = list.stream()
                .filter(word -> word.getWord().matches(RegexConstants.VOWEL_WORDS))
                .collect(Collectors.toList());
        return wordsOnVowel.get(0).getWord();
    }

    public String longestWord(List<Word> list) {
        List<Word> longestWord = list.stream()
                .sorted(new StringLengthComparator())
                .collect(Collectors.toList());
        return longestWord.get(longestWord.size() - 1).getWord();
    }
}
