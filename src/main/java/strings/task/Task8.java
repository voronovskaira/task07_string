package strings.task;

import strings.utils.PropertiesHandler;

public class Task8 implements Task {
    @Override
    public void executeTask() {
        PropertiesHandler.setLocale("uk");
    }
}
