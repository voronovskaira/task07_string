package strings.utils;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import strings.task.Task4;

import java.util.Scanner;

public class ConsoleReader {

    private static Logger LOG = LogManager.getLogger(Task4.class);

    public String scanFromConsole(String message) {
        Scanner scan = new Scanner(System.in);
        LOG.info(message);
        return scan.nextLine();
    }

    public int readValue(String message, int boarder) {
        try {
            int result = Integer.parseInt(scanFromConsole(message));
            if (checkRange(result, boarder))
                return result;
        } catch (NumberFormatException e) {
            LOG.info("INCORRECT_VALUE. TRY AGAIN!");
        }
        return readValue(message, boarder);
    }

    private boolean checkRange(int value, int range) throws NumberFormatException {
        if (value > 0 && value <= range) {
            return true;
        } else throw new NumberFormatException();
    }
}
