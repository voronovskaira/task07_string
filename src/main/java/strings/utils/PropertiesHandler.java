package strings.utils;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Properties;

public class PropertiesHandler {
    private static String value;
    private static String FILE_PATH = "src/main/resources/config.properties";
    private static Properties property = new Properties();

    public static String getLocale() {
        try {
            FileInputStream file = new FileInputStream(FILE_PATH);
            property.load(file);
            value = property.getProperty("locale");
        } catch (IOException e) {
            System.err.println("Error");
        }
        return value;
    }

    public static void setLocale(String locale){
        try (Writer inputStream = new FileWriter(FILE_PATH)) {
            property.setProperty("locale", locale);
            property.store(inputStream, "Change locale");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
