package strings.utils;

import strings.constant.RegexConstants;
import strings.model.Sentence;
import strings.model.Word;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TextParser {
    public List<Sentence> findSentencesInText(String text) {
        List<Sentence> sentences = new ArrayList<>();
        Pattern pattern = Pattern.compile(RegexConstants.SENTENCE_REGEX);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            sentences.add(new Sentence(matcher.group().trim(), findWordsInSentence(matcher.group().trim())));
        }
        return sentences;
    }

    private List<Word> findWordsInSentence(String text) {
        List<Word> words = new ArrayList<>();
        Pattern pattern = Pattern.compile(RegexConstants.WORD_REGEX);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            List<Character> wordDescription = matcher.group().chars().mapToObj(c -> (char) c).collect(Collectors.toList());
            Word word = new Word(matcher.group(), wordDescription);
            words.add(word);
        }
        return words;
    }


}
