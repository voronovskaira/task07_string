package strings.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TextReader {
    private static String value;

    public String readFirstLineFromFile(String path) {
        StringBuilder text = new StringBuilder();
        try (BufferedReader br =
                     new BufferedReader(new FileReader(path))) {
            br.lines().forEach(x -> text.append(x).append("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text.toString();
    }
}
